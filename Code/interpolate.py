import numpy as np 
import mrcfile
import cv2
import glob
import os 

for mrc_path in glob.glob('/hpc/home/qh36/research/qh36/csplus/srcryoem/ctf_sr/apoferritin/*.mrc'):
	print(mrc_path)
	print(mrc_path[-55:-10])
	name = mrc_path[-55:-10]+'bicubic.mrc'
	print(name)
	with mrcfile.open(mrc_path) as mrc:
		dat = mrc.data
	img_resized = cv2.resize(dat, dsize = (int(dat.shape[0]*2),int(dat.shape[1]*2)), interpolation=cv2.INTER_CUBIC)
	path_new_name = os.path.join('/hpc/home/qh36/research/qh36/csplus/srcryoem/ctf_sr/bicubic/', name)
	with mrcfile.new(path_new_name) as mrc:
		mrc.set_data(img_resized)


