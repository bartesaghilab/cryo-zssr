import torch
from torch import nn



class GeneratorLoss(nn.Module):
	def __init__(self, model_path, device):
		super(GeneratorLoss, self).__init__()
		ctf_model = regression_model.ResidualLowpass()
		ctf_model.to(device)
		checkpoint = torch.load(model_path)
		ctf_model.load_state_dict(checkpoint['model_state_dict'])
		ctf_model = ctf_model.to(device)
		self.loss_network = ctf_model
		self.mse_loss = nn.MSELoss()
		self.tv_loss = TVLoss()

	def forward(self, out_images, target_images, out_ctf):
		# print('out_ctf shape')
		# print(out_ctf.shape)
		ctf_loss = 0.6 - self.loss_network(out_ctf)
		
		image_loss = self.mse_loss(out_images, target_images)
		tv_loss = self.tv_loss(out_images)
		return image_loss + 10*ctf_loss + 2e-8 * tv_loss
class GeneratorLoss2(nn.Module):
	def __init__(self, model_path, device):
		super(GeneratorLoss2, self).__init__()
		# ctf_model = regression_model.ResidualLowpass()
		# ctf_model.to(device)
		# checkpoint = torch.load(model_path)
		# ctf_model.load_state_dict(checkpoint['model_state_dict'])
		# ctf_model = ctf_model.to(device)
		# self.loss_network = ctf_model
		self.mse_loss = nn.MSELoss()
		self.tv_loss = TVLoss()

	def forward(self, out_images, target_images,lr_est, lr_img, last_frame):

		image_loss = self.mse_loss(out_images, target_images)
		tv_loss = self.tv_loss(out_images)
		if last_frame:
			flow_loss = 0
		else:
			flow_loss = self.mse_loss(lr_est, lr_img)
		return image_loss + 2e-8 * tv_loss + 0.01*flow_loss

class GeneratorLoss3(nn.Module):
	def __init__(self, device):
		super(GeneratorLoss3, self).__init__()
		self.mse_loss = nn.MSELoss()
		self.tv_loss = TVLoss()

	def forward(self, out_images, target_images):
		image_loss = self.mse_loss(out_images, target_images)
		# low_loss = self.mse_loss(out_lowpass, target_lowpass)
		tv_loss = self.tv_loss(out_images)
		return image_loss + 2e-8 * tv_loss


class NormalLoss(nn.Module):
	def __init__(self, model_path, device):
		super(NormalLoss, self).__init__()
		ctf_model = regression_model.ResidualLowpass()
		ctf_model.to(device)
		checkpoint = torch.load(model_path)
		ctf_model.load_state_dict(checkpoint['model_state_dict'])
		ctf_model = ctf_model.to(device)
		self.loss_network = ctf_model
		self.mse_loss = nn.MSELoss()
		self.tv_loss = TVLoss()

	def forward(self, out_images, target_images, out_ctf):
		#ctf_loss = 1 - self.loss_network(out_ctf)
		image_loss = self.mse_loss(out_images, target_images)
		tv_loss = self.tv_loss(out_images)
		return image_loss + 2e-8 * tv_loss


class L1_loss(nn.Module):
	def __init__(self, model_path, device):
		super(L1_loss, self).__init__()
		ctf_model = regression_model.ResidualLowpass()
		ctf_model.to(device)
		checkpoint = torch.load(model_path)
		ctf_model.load_state_dict(checkpoint['model_state_dict'])
		ctf_model = ctf_model.to(device)
		self.loss_network = ctf_model
		self.l1loss = nn.L1Loss()
		# self.mse_loss = nn.MSELoss()
		# self.tv_loss = TVLoss()

	def forward(self, out_images, target_images, out_ctf):
		print('out_ctf shape')
		print(out_ctf.shape)
		#ctf_loss = 1 - self.loss_network(out_ctf)
		#image_loss = self.mse_loss(out_images, target_images)
		image_loss = self.l1loss(out_images, target_images)
		#tv_loss = self.tv_loss(out_images)
		return image_loss
class TVLoss(nn.Module):
	def __init__(self, tv_loss_weight=1):
		super(TVLoss, self).__init__()
		self.tv_loss_weight = tv_loss_weight

	def forward(self, x):
		batch_size = x.size()[0]
		h_x = x.size()[2]
		w_x = x.size()[3]
		count_h = self.tensor_size(x[:, :, 1:, :])
		count_w = self.tensor_size(x[:, :, :, 1:])
		h_tv = torch.pow((x[:, :, 1:, :] - x[:, :, :h_x - 1, :]), 2).sum()
		w_tv = torch.pow((x[:, :, :, 1:] - x[:, :, :, :w_x - 1]), 2).sum()
		return self.tv_loss_weight * 2 * (h_tv / count_h + w_tv / count_w) / batch_size

	@staticmethod
	def tensor_size(t):
		return t.size()[1] * t.size()[2] * t.size()[3]

if __name__=="__main__":
	if torch.cuda.is_available():
		device = torch.device('cuda:0')
		print('Running on GPU, device:', torch.cuda.current_device(), 'name:', torch.cuda.get_device_name(torch.cuda.current_device()))
	else:
		device = torch.device('cpu')
		print('Running on CPU')
	checkpoint_path = '/hpc/home/qh36/research/qh36/csplus/srcryoem/regression_models/cp-ctf_model_2/79-0.9622499602032054.tar'
	g_loss = GeneratorLoss(checkpoint_path, device)
	print(g_loss)

